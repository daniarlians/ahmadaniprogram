
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  Alert,
} from 'react-native';

export default class reactTutorialApp extends Component {

   constructor(props) {
    super(props);
    this.state = { email: 'Masukkan Username                  ', passowrd: 'Masukkan Npm                             ' };
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Silahkan Login Untuk Ngapsen
          Di Kelas TI_B
        </Text>
      
        <TextInput
          style={{height: 40, margin:20, padding:10, borderColor: 'gray', borderWidth: 1}}
          onChangeText={(text) => this.setState({text})}
          value={this.state.email}
        />
        <TextInput
          style={{height: 40, margin:20, padding:10, borderColor: 'gray', borderWidth: 1}}
          onChangeText={(text) => this.setState({text})}
          value={this.state.passowrd}
        />
        <Button
          onPress={onButtonPress}
          title="Login"
          color="#0303fc"
        />
      </View>
    );
  }
}

const onButtonPress = () => {
  Alert.alert('Login was clicked!');
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#bdd9ff',

  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    color:"#0303fc",

  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

AppRegistry.registerComponent('reactTutorialApp', () => reactTutorialApp);
